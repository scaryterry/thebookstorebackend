1. Install Nodemon as a global service (npm install -g nodemon).

2. Navigate to the folder containing the package.json file and run npm install to install all dependencies of the project.

3. Navigate to the folder containing the app.js file, Open a Command window and type nodemon to start the server.

4. Make sure the React Frontend is running to get the application working properly.

Make sure both the console windows of react server and node are running when testing the application.
NOTE: CORS Chrome Extension might be needed for Cross Origin Reference issues. Install CORS and enable the same for proper working.
