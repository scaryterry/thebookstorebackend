var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs");

var booksRouter = express.Router();

booksRouter.use(bodyParser.json());

booksRouter
  .route("/")

  .get(function(req, res, next) {
    let data = fs.readFileSync("db.json");
    arr = JSON.parse(data);
    res.end(JSON.stringify(arr.books));
  });

module.exports = booksRouter;
