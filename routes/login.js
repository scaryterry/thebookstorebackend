var express = require("express");
var bodyParser = require("body-parser");
var loginRouter = express.Router();
var fs = require("fs");

loginRouter.use(bodyParser.json());

loginRouter
  .route("/")
  .post(function(req, res, next) {
    usr = req.body[0].username;
    pw = req.body[0].password;
    var dataout = [];
    fs.readFile("db.json", function(err, data) {
      data = JSON.parse(data);
      dataout = data.users.filter(myuser => myuser.username == usr);
      if (dataout.length == 0) {
        var err = new Error("Not Authrorized");
        err.status = 401;
        next(err);
      } else {
        if (usr == dataout[0].username && pw == dataout[0].password) {
          loginInfo = [
            {
              username: usr,
              id: dataout[0].id,
              loggedin: true
            }
          ];
          res.end(JSON.stringify(loginInfo));
        } else {
          var err = new Error("Username or Password Error!");
          err.status = 401;
          next(err);
        }
      }
    });
  })

  //This is a Logout Function
  .get(function(req, res, next) {
    loginInfo = [];
    res.end(JSON.stringify(loginInfo));
  });

module.exports = loginRouter;
