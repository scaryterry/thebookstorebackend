var express = require("express");
var userRouter = express.Router();
var bodyParser = require("body-parser");
var fs = require("fs");

userRouter.use(bodyParser.json());

userRouter
  .route("/")
  .get(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      data = JSON.parse(data);
      res.end(JSON.stringify(data.users));
    });
  })

  .post(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      jsonfile = JSON.parse(data);
      jsonfile.users.push(req.body);
      let write = JSON.stringify(jsonfile, null, 2);
      fs.writeFile("db.json", write, err => {
        if (err) res.end(err);
      });
      fs.readFile("db.json", (err, data) => {
        data = JSON.parse(data);
        res.end(JSON.stringify(data.users));
      });
    });
  })

  .delete(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      jsonfile = JSON.parse(data);
      jsonfile.users = [];
      let write = JSON.stringify(jsonfile, null, 2);
      fs.writeFile("db.json", write, err => {
        if (err) res.end(err);
      });
      fs.readFile("db.json", (err, data) => {
        data = JSON.parse(data);
        res.end(JSON.stringify(data.users));
      });
    });
  });

userRouter
  .route("/:userId/cart")
  .get(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      jsonfile = JSON.parse(data);
      carts = jsonfile.users.filter(user => user.id == req.params.userId);
      if (carts.length === 0) {
        err = new Error("Unauthorized!");
        err.status = 401;
        res.end(JSON.stringify(err));
      } else {
        carts = carts[0].cart;
        res.end(JSON.stringify(carts));
      }
    });
  })
  .post(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      jsonfile = JSON.parse(data);
      updatecarts = jsonfile.users.filter(user => user.id == req.params.userId);
      updatecarts[0].cart.push(req.body);
      carts = jsonfile.users.filter(user => user.id != req.params.userId);
      carts.push(updatecarts[0]);
      jsonfile.users = carts;
      let write = JSON.stringify(jsonfile, null, 2);
      fs.writeFile("db.json", write, err => {
        if (err) res.end(err);
      });
      res.end(JSON.stringify(carts));
    });
  })
  .delete(function(req, res, next) {
    fs.readFile("db.json", function(err, data) {
      jsonfile = JSON.parse(data);
      updatecarts = jsonfile.users.filter(user => user.id == req.params.userId);
      updatecarts[0].cart = [];
      carts = jsonfile.users.filter(user => user.id != req.params.userId);
      carts.push(updatecarts[0]);
      jsonfile.users = carts;
      let write = JSON.stringify(jsonfile, null, 2);
      fs.writeFile("db.json", write, err => {
        if (err) res.end(err);
      });
      res.end(JSON.stringify(carts));
    });
  });

userRouter.route("/:userId/cart/:bookId").delete(function(req, res, next) {
  fs.readFile("db.json", function(err, data) {
    jsonfile = JSON.parse(data);
    updatecarts = jsonfile.users.filter(user => user.id == req.params.userId);
    var pos = updatecarts[0].cart
      .map(function(e) {
        return e.bookid;
      })
      .indexOf(parseInt(req.params.bookId, 10));
    if (pos != -1) updatecarts[0].cart.splice(pos, 1);
    carts = jsonfile.users.filter(user => user.id != req.params.userId);
    carts.push(updatecarts[0]);
    jsonfile.users = carts;

    let write = JSON.stringify(jsonfile, null, 2);
    fs.writeFile("db.json", write, err => {
      if (err) res.end(err);
    });
    res.end(JSON.stringify(updatecarts[0].cart));
  });
});

module.exports = userRouter;
